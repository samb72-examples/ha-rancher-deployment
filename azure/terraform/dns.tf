resource "azurerm_dns_a_record" "vm_dns_record" {
  count               = "${var.lab_size}"
  name                = "${var.resource_group}-${count.index + 1}"
  zone_name           = "${var.dns_zone_details["zone"]}"
  resource_group_name = "${var.dns_zone_details["resource_group"]}"
  ttl                 = 300
  records             = [["${element(azurerm_public_ip.vm_public_ip.*.ip_address, count.index)}"]]

  depends_on = ["azurerm_virtual_machine.vm"]
}

resource "azurerm_dns_a_record" "lb_dns_record" {
  name                = "rancher-${var.resource_group}"
  zone_name           = "${var.dns_zone_details["zone"]}"
  resource_group_name = "${var.dns_zone_details["resource_group"]}"
  ttl                 = 300
  records             = ["${azurerm_public_ip.lb_public_ip.ip_address}"]

  depends_on = ["azurerm_lb.rancher_lb"]
}
