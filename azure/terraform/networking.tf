resource "azurerm_virtual_network" "vnet" {
  name                = "${var.region}-vnet"
  resource_group_name = "${azurerm_resource_group.lab_resource_group.name}"
  address_space       = ["${var.address_space}"]
  location            = "${var.region}"
}

resource "azurerm_subnet" "subnet" {
  name                 = "${var.region}-subnet"
  resource_group_name  = "${azurerm_resource_group.lab_resource_group.name}"
  virtual_network_name = "${azurerm_virtual_network.vnet.name}"
  address_prefix       = "${var.address_space}"
}

resource "azurerm_network_interface" "nic" {
  count               = "${var.lab_size}"
  name                = "${var.resource_group}-${count.index + 1}"
  location            = "${azurerm_resource_group.lab_resource_group.location}"
  resource_group_name = "${azurerm_resource_group.lab_resource_group.name}"

  ip_configuration {
    name                                    = "testconfiguration1"
    subnet_id                               = "${azurerm_subnet.subnet.id}"
    private_ip_address_allocation           = "Static"
    private_ip_address                      = "10.0.0.${10 + count.index}"
    load_balancer_backend_address_pools_ids = ["${azurerm_lb_backend_address_pool.rancher_lb_backend.id}"]
    public_ip_address_id                    = "${element(azurerm_public_ip.vm_public_ip.*.id, count.index)}"
  }
}

resource "azurerm_public_ip" "vm_public_ip" {
  count                        = "${var.lab_size}"
  name                         = "${azurerm_resource_group.lab_resource_group.name}-vm-public-ip-${count.index + 1}"
  resource_group_name          = "${azurerm_resource_group.lab_resource_group.name}"
  location                     = "${var.region}"
  public_ip_address_allocation = "Dynamic"
}

# Load balancer config

resource "azurerm_public_ip" "lb_public_ip" {
  name                         = "${azurerm_resource_group.lab_resource_group.name}-lb-public-ip"
  resource_group_name          = "${azurerm_resource_group.lab_resource_group.name}"
  location                     = "${var.region}"
  public_ip_address_allocation = "Dynamic"
}

resource "azurerm_lb" "rancher_lb" {
  name                = "${azurerm_resource_group.lab_resource_group.name}-rancher-lb"
  location            = "${var.region}"
  resource_group_name = "${azurerm_resource_group.lab_resource_group.name}"

  frontend_ip_configuration {
    name                 = "Rancher-FrontEnd"
    public_ip_address_id = "${azurerm_public_ip.lb_public_ip.id}"
    private_ip_address   = "10.0.0.100"
  }
}

resource "azurerm_lb_rule" "rancher_lb_rule" {
  resource_group_name            = "${azurerm_resource_group.lab_resource_group.name}"
  loadbalancer_id                = "${azurerm_lb.rancher_lb.id}"
  name                           = "Rancher-LB-Incoming-8080"
  protocol                       = "Tcp"
  frontend_port                  = 443
  backend_port                   = 443
  frontend_ip_configuration_name = "Rancher-FrontEnd"
  backend_address_pool_id        = "${azurerm_lb_backend_address_pool.rancher_lb_backend.id}"
  probe_id                       = "${azurerm_lb_probe.rancher_lb_probe.id}"
}

resource "azurerm_lb_backend_address_pool" "rancher_lb_backend" {
  resource_group_name = "${azurerm_resource_group.lab_resource_group.name}"
  loadbalancer_id     = "${azurerm_lb.rancher_lb.id}"
  name                = "Rancher-Backend"
}

resource "azurerm_lb_probe" "rancher_lb_probe" {
  resource_group_name = "${azurerm_resource_group.lab_resource_group.name}"
  loadbalancer_id     = "${azurerm_lb.rancher_lb.id}"
  name                = "Rancher-Probe"
  port                = 80
  protocol            = "http"
  request_path        = "/healthz"
}
