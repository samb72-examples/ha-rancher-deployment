output "vms" {
  value = "${join(",",azurerm_virtual_machine.vm.*.name)}"
}

output "ips" {
  value = "${join(",",azurerm_public_ip.vm_public_ip.*.ip_address)}"
}

output "dnsnames" {
  value = "${join(",",azurerm_dns_a_record.vm_dns_record.*.name)}"
}

output "dnszone" {
  value = "${var.dns_zone_details["zone"]}"
}
