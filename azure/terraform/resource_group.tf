resource "azurerm_resource_group" "lab_resource_group" {
  name     = "lab-${var.resource_group}"
  location = "${var.region}"
}
